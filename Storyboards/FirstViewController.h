//
//  FirstViewController.h
//  Storyboards
//
//  Created by Paul Robichaud on 11/30/13.
//  Copyright (c) 2013 Paul Robichaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *firstTabTextField;
- (IBAction)commitText:(id)sender;

@end
