//
//  AppDelegate.h
//  Storyboards
//
//  Created by Paul Robichaud on 11/30/13.
//  Copyright (c) 2013 Paul Robichaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *firstTabText;

@end
