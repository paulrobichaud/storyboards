//
//  SecondViewController.h
//  Storyboards
//
//  Created by Paul Robichaud on 11/30/13.
//  Copyright (c) 2013 Paul Robichaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *secondTabTextField;
- (IBAction)commitText:(id)sender;


@end
